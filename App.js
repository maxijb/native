import React from 'react';
import { StyleSheet, Text, View, TextInput, Alert, Button } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "Input goes here"
    }
  }

  _onPressButton() {
    Alert.alert('You tapped the button!')
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{flex: 1, backgroundColor: 'powderblue'}}>
        </View>
        <View style={{flex: 2, backgroundColor: 'red'}}>
          <TextInput
            style={{height: 40, backgroundColor: 'white'}}
            placeholder="Type here to translate!"
            onChangeText={(text) => this.setState({text})}
          />
          <Button
            onPress={this._onPressButton}
            title="Press Me"
          />
          <Text style={styles.text}>Open up App.js to start working on your app, Maxi!</Text>
        </View>
        <View style={{flex: 3, backgroundColor: 'steelblue', width: 150 }}>
          <Text style={styles.text}>{this.state.text}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
    justifyContent: 'center',
    padding: 0
  },
  text: {
    color: '#fff'
  }
});
